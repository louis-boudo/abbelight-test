#include <string>
#include "vector"
using namespace std;

typedef vector<vector<int>> Grid;
vector<vector<int>> readFile(string filename);
void showGrid(Grid grid );
bool doSurvive(int i, int j, const Grid grid );
void mainLoop(Grid grid,int iter,bool doShowGrid);