#include"functions.h"
#include<iostream>
#include<string>
#include<cassert>
#include<vector>
using namespace std;
vector<vector<int>> gridTest{
        {0,1,0,0},
        {1,1,0,1},
        {0,1,0,0}
    };
void checkShowGrid(){
    cout<<"Grid show test" << endl;
    showGrid(gridTest);
    cout << "// must be like ->"<<endl;
    cout << "_*__\n**_*\n_*__"<<endl;
}
void checkReadFile(){
    vector<vector<int>> gridToCheck = readFile("../data/test.txt");
    assert(gridToCheck==gridTest);
}
void checkSurvive(){
    assert(doSurvive(0,0,gridTest)==true);
    assert(doSurvive(1,1,gridTest)==true);
    assert(doSurvive(2,3,gridTest)==false);
}

int main(int argc, char const *argv[])
{
    checkShowGrid();
    checkReadFile();
    checkSurvive();
    return 0;
}
