#include"functions.h"
#include<iostream>
#include<string>
#include<cassert>
#include<vector>
#include <argparse/argparse.hpp>

using namespace std;

int main(int argc, char const *argv[])
{
    /* code */
    argparse::ArgumentParser program("GameOfLife");

    program.add_argument("--input")
    .required()
    .help("File input path");

    program.add_argument("--iterations")
    .required()
    .help("Number of iterations")
    .scan<'i',int>()
    .default_value(10);

    program.add_argument("--all")
    .help("Show grid at each iterations")
    .default_value(false)
    .implicit_value(true);
    
    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        return 1;
    }

    string filename = program.get<string>("--input");
    int interations = program.get<int>("--iterations");
    bool isShow = program.get<bool>("--all");
    Grid grid = readFile(filename);
    showGrid(grid);
    mainLoop(grid,interations,isShow);
    return 0;
}
