#include<iostream>
#include<fstream>
#include "functions.h"
using namespace std;
/**
 * @brief Reads file and parse it into a Grid
 * 
 * @param filename 
 * @return Grid 
 */
Grid readFile(string filename){
    string line;
    ifstream fileObj(filename);

    if(fileObj.fail()){
        throw invalid_argument("Your file is invalid."); 
    }

    int current_char;
    int lineSize = -1;
    Grid grid {};

    while(getline(fileObj,line)){
        vector<int> current_line {};
        for(const char c:line){
            if(c=='_'){
                current_char=0;
            }else if(c=='*'){
                current_char=1;
            }
            else{
                throw invalid_argument("You have an invalid character in your file."); 
            }
            current_line.push_back(current_char);
        }
        if(lineSize<0){
            lineSize=current_line.size();
        }
        else if(lineSize != current_line.size()){
            throw invalid_argument("Your file has inconsitent line lengths."); 
        }
        grid.push_back(current_line);

    }
    if(grid.size() < 1){
        throw invalid_argument("Your file is empty."); 
    }
    return grid;
}

/**
 * @brief 
 * 
 * @param grid 
 */
void showGrid(const Grid grid ){
    for(int i = 0;i<grid.size();i++){
        for(const int j : grid[i]){
            cout << (j==0 ? '_':'*');
        }
        cout << endl;
    }
}

/**
 * @brief Compute whenever the current position stay alive at the next iteration
 * 
 * @param i row index 
 * @param j line index
 * @param grid grid object

 * @return bool if its alive or not 
 */
bool doSurvive(int i, int j, const Grid grid ){
    // We have each possible position aroud our point
    int positions[9][2] = {
        {i+1,j},
        {i+1,j+1},
        {i+1,j-1},

        {i,j+1},
        {i,j-1},

        {i-1,j},
        {i-1,j+1},
        {i-1,j-1}       
    };
    int sum = 0;
    int rowSize = grid.size();
    int lineSize = grid[0].size();
    for(const int* pos : positions){
        // We check if we have one the around position that goes out of the grid
        // If so, the dont compute
        if(pos[0]>=0 && pos[1]>=0 && pos[0]<rowSize && pos[1]<lineSize){
            // As the grid is composed of 0 and 1, summing gives the number of alive cells
            sum+=grid[pos[0]][pos[1]];
        }
    }
    if(grid[i][j]){
        return sum == 3 || sum == 2 ;
    }else{
        return sum == 3 ;
    }
}

/**
 * @brief The main loop that compute the game of life
 * 
 * @param grid initial grid
 * @param iter number of iterations
 * @param doShowGrid if we show or not the grid at each iterations
 */
void mainLoop(Grid grid,int iter,bool doShowGrid){

    int rowSize = grid.size();
    int lineSize = grid[0].size();
    Grid newGrid(rowSize);

    for(int k = 0; k<iter ; k++){
        for(int i =0 ;i<rowSize; i++){
            newGrid[i] = vector<int>(lineSize);
            for(int j=0 ;j<lineSize; j++){
                newGrid[i][j]=doSurvive(i,j,grid);
            }
        }
        grid = newGrid;
        if(doShowGrid){
            cout << "/// iteration no : "<< k+1 << endl;
            showGrid(grid);
            cout << "///////////////" << endl;

        }
    }

    cout << "/// iteration no : Final result" << endl;
    showGrid(grid);
    cout << "///////////////" << endl;
}