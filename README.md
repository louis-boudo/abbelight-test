# Abbelight test / Game of life in c\++
This is my submission for the game of life test


## Getting started

compile by executing these commands at root directory:
```
mkdir build
cd build
cmake ..
make
```
to run test, go in build directory and execute:
```
./tests
```
to run go in build directory and execute:
```
./gameOfLife --input myfile.txt --iterations 100 --all
```
## File format

**_** represents a dead cell and **\*** represents a live cell, no other characters other that these two

You need to have the same number of characters at each line and nothing else but the grid is represented in your file.

### exemple
```
__**__
___*_*
*___**
```
## Arguments

***--input*** is the input file of the initial state, mandatory

***--iterations*** is the input file of the initial state, optional and default = *10*

***--all*** whenever we show grid at each iterations, mandatory, default = *false*, just put the argument for *true* and omit it for *false*

### exemples
```
./gameOfLife --input myfile.txt --iterations 10 --all
./gameOfLife --input myfile.txt --iterations 10
./gameOfLife --input myfile.txt
./gameOfLife --input myfile.txt --all

```
